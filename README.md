# LevelManager

//Level Manager was added throughout all the scenes and coded accordingly for when certain things occur. Example player gets a certain amount of score or she survives the timer

public class LevelManager : MonoBehaviour
{
    public void LoadLevel(string name)
    {
        SceneManager.LoadScene(name);
    }

    public void QuitEditor()
    {
        #if UNITY_EDITOR
                        UnityEditor.EditorApplication.isPlaying = false;
        #else
                Application.Quit();
        #endif
    }
}
